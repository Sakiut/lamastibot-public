"""
Lamastibot — Discord permissions's check lib

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

from discord.ext import commands
import discord.utils


def is_owner_check(message):
    return message.author.id == 187565415512276993


def is_owner():
    return commands.check(lambda ctx: is_owner_check(ctx.message))


def is_channel(channel_id):
    def predicate(ctx):
        return ctx.channel.id == channel_id
    return commands.check(predicate)


def check_permissions(ctx, perms):
    msg = ctx.message
    if is_owner_check(msg):
        return True

    ch = msg.channel
    author = msg.author
    resolved = ch.permissions_for(author)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())


def role_or_permissions(ctx, check, **perms):
    if check_permissions(ctx, perms):
        return True

    ch = ctx.message.channel
    author = ctx.author
    if isinstance(ch, (discord.DMChannel, discord.GroupChannel)):
        return False  # can't have roles in PMs

    role = discord.utils.find(check, author.roles)
    if role is None:
        raise commands.CommandError("You need a special role to do this! (admin, Gardien)")
    return True


def gardien_or_permissions():
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.id == 251419879951958027, manage_roles=True)
    return commands.check(predicate)


def admin_or_permissions():
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.id == 220310157513457664, administrator=True)
    return commands.check(predicate)


def nsfw_channel():
    # noinspection PyUnresolvedReferences
    def predicate(ctx):
        if not (isinstance(ctx.channel,
                           (discord.DMChannel, discord.GroupChannel)) or "nsfw" in ctx.channel.name.casefold()):
            raise ChannelError("This command can only be used in `nsfw` channels!")
        return True

    return commands.check(predicate)


def no_pm():
    def predicate(ctx):
        if ctx.command.name == "help":
            return True
        if ctx.guild is None:
            raise commands.NoPrivateMessage('This command cannot be used in private messages.')
        return True

    return commands.check(predicate)
