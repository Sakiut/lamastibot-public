# Lamastibot

This repository contains the Public part of the Corobizar's Lamastibot (Discord bot).

## Requirements

```
python==3.6.*
discord.py==1.2.*
aiohttp
pytz
mysqlclient
pillow
```

## Config.txt

```shell
# Discord tokens

lamastibot_token=LamastibotDiscordToken
fourasticot_token=FourasticotDiscordToken

# Twitch App Logs

client_id=TwitchClientId
secret_key=TwitchSecretKey

# DB credentials

db_host=DataBaseHost
db_token=DataBaseToken

# Roles attribution message
roles_msg=MessageID
```

## License

```
Lamastibot & Fourasticot

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). You 
are free to modify this Software and publish patches to the Software. You agree 
that Sakiut retain all right, title and interest in and to all such modifications 
and/or patches, and all such modifications and/or patches may only be used, 
copied, modified, displayed, distributed, or otherwise exploited with the 
authorization of the author. Notwithstanding the foregoing, you may not copy 
and modify the Software for development and testing purposes, without 
requiring a subscription. You agree that Sakiut retain all right, title and 
interest in and to all such modifications. You are not granted any other rights 
beyond what is expressly stated herein. Subject to the foregoing, it is 
forbidden to copy, merge, publish, distribute, sublicense, and/or sell the 
Software.

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
```
