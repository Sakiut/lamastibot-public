"""
Fourasticot — Main

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

import discord
import asyncio
from discord.ext import commands
from datetime import datetime, timedelta

from libs import captcha as captcha_mgr
from libs import lib, db


print(f"[F][{datetime.now().strftime('%x %X')}] Connecting...")


class Bot(commands.AutoShardedBot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, activity=discord.Game(name="À quand la prochaine épreuve ?"), **kwargs)

        self.owner_id = 187565415512276993
        self.link_channel_id = 484481286015287297
        self.lamastibot_id = 352425465195134976

        self.uptime = datetime.now()

    async def on_ready(self):
        print('-------------------------------------------------------------')
        print(f'[F][{datetime.now().strftime("%x %X")}] Logged in as')
        print(f'[F][{datetime.now().strftime("%x %X")}]', self.user.name)
        print(f'[F][{datetime.now().strftime("%x %X")}]', self.user.id)
        print('-------------------------------------------------------------')

    async def on_message(self, message):

        if not message.channel.id == self.link_channel_id:
            return

        if message.author.id not in (self.owner_id, self.lamastibot_id):
            return

        task_type = message.content.split(":")[0]

        if task_type == "start":
            content = message.content.split(":")[1].split("|-@-|")
            data = {
                "channel_id": int(content[0]),
                "captcha_id": content[1],
            }

            channel = self.get_channel(data["channel_id"])
            captcha = captcha_mgr.get_captcha(data['captcha_id'])

            def check(m):
                return m.channel == channel

            await channel.send("<:FOURASTICOT:230222304737492992> Voici mon épreuve, à vos claviers ! "
                               "<:FOURASTICOT:230222304737492992>",
                               file=discord.File(captcha),
                               delete_after=600)
            end_time = datetime.now() + timedelta(seconds=8)

            results = []
            losers = []

            try:
                while datetime.now() < end_time:
                    rs = await self.wait_for("message", check=check, timeout=8)
                    if lib.is_time_master(rs.author, rs.guild):
                        await channel.send(f"Tu as déjà réussi mon épreuve {rs.author.mention}, laisse les autres "
                                           f"tenter leur chance !  <:yee:293391531237441537>",
                                           delete_after=600)
                        continue
                    if rs.content == data["captcha_id"]:
                        results.append(rs.author)
                    else:
                        continue
            except asyncio.TimeoutError:    # This should never happen
                pass

            end_time = datetime.now() + timedelta(seconds=15)

            try:
                while datetime.now() < end_time:
                    rs = await self.wait_for("message", check=check, timeout=15)
                    if rs.content == data["captcha_id"]:
                        losers.append(rs.author)
                    else:
                        continue
            except asyncio.TimeoutError:    # This should never happen
                pass

            if len(results) == 0:
                await channel.send("Aucun disciple n'a réussi à me vaincre, entraînez-vous d'ici mon prochain passage !"
                                   " <:CACA:230222568441774081>",
                                   delete_after=600)
                for user in losers:
                    await db.lose_rename(user, lib.get_role(message.guild, 487642902604939264))

                return

            winner = results[0]

            await winner.add_roles(lib.get_time_master(channel.guild))
            await db.add_xp(winner, 750)
            await channel.send(f"""Nous avons notre gagnant ! GG {winner.mention}, tu as réussi mon épreuve ! 
Voici tes récompenses : <:FOURASTICOT:230222304737492992>
- Le grade "Maître du Temps"
- 750 points d'expérience
- La couleur "Orange" """,
                               delete_after=600)

            winner_profile = await db.get_profile(user=winner)
            winner_profile["time_master"] = True
            await db.update_profile(winner.id, winner_profile)

            del results[0]

            for user in results:
                await db.lose_rename(user, lib.get_role(message.guild, 487642902604939264))

            for user in losers:
                await db.lose_rename(user, lib.get_role(message.guild, 487642902604939264))

        await self.process_commands(message)


token = lib.get_fourasticot_token()
bot = Bot(command_prefix=commands.when_mentioned_or(".!"), description="Commandes Fourasticot", pm_help=True)
bot.run(token)
